Web Developer - Technical Test
==============================
Thank you for applying for a Web Developer position at Wholegrain Digital. We have prepared a short developer test for you to complete before you interview with us. This will help us assess if the role is a good match for your skills and interests. Our technical background is the usual LAMP environment, and our websites are mainly WordPress custom theme builds using Webpack, gulp, sass and es6.

Frontend Test
-------------
Write a HTML and CSS file that will output the included mockup. The mockup has been provided as a pdf as well as a sketch file. Additionally, assets that you can use to produce the mockup have been included.

Aligning with our ethos, we are interested in scalable, maintainable and efficient code and an implementation that avoids the use of frameworks.

Backend Test
------------
We would like you to show your understanding of PHP & WordPress by building a theme that exposes a custom post type that any front-end client can consume. You are welcome to use WordPress plugins if you feel they are necessary.

To give you an example, we would like to see something that adheres to (doesn’t need to be exact) the following guidelines:

 1. Create a new WordPress theme
     2. There will be no *front-end* to the theme (no need to create any templates).
     3. It’s only purpose is to expose data that can be accessed via AJAX.
 4. Register a custom post type called *Recipes* with support for
     5. Title (string)
     6. WYWISYG
     7. Category (can be the default WordPress post category taxonomy)
     8. Ingredients (custom fields, wysiwyg)
 13. Expose the *Recipes* post type in a manner that can be consumed by an external application
     14. The title, the content, the ingredients, and the permalink must be exposed

To demonstrate additional knowledge, provide code that consumes the exposed *recipes*.

General Questions
------------------
 1. Tell us one thing you find most websites are doing poorly? How would you fix it?
 2. One of our staff is facing a bug on the website you cannot reproduce. How would you proceed?

Notes
--------
- Please spend no more than 2 hours
- Assets have been provided, use them if required
- We are not expecting the same font, or a pixel perfect replication, but do your best to make it consistent
- You are more than welcome to use your favourite build tools
- Please submit the work to a git repository and share the link with us